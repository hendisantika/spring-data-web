package com.hendisantika.springdataweb.controller;

import com.hendisantika.springdataweb.entity.Book;
import com.hendisantika.springdataweb.repository.BookRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.HttpStatus.OK;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-data-web
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 10/08/18
 * Time: 07.14
 * To change this template use File | Settings | File Templates.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = RANDOM_PORT)
public class BookControllerTest {
    @Autowired
    private BookRepository repository;

    @Autowired
    private TestRestTemplate restTemplate;

    @Before
    public void setUp() {
        repository.save(
                new Book("Spring Data Rest With Web Demo")
        );
    }

    @Test
    public void get() {
        ResponseEntity<Book> response = restTemplate.getForEntity("/book/{id}", Book.class, 1L);

        assertThat(response.getStatusCode())
                .isEqualTo(OK);

        assertThat(response.getBody().getTitle())
                .isEqualTo("Spring Data Rest With Web Demo");
    }

    @Test
    public void getWithNonExistingId() {
        ResponseEntity<Book> response = restTemplate.getForEntity("/book/{id}", Book.class, 100L);

        assertThat(response.getStatusCode())
                .isEqualTo(NOT_FOUND);
    }

}