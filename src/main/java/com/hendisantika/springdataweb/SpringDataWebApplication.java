package com.hendisantika.springdataweb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringDataWebApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringDataWebApplication.class, args);
    }
}
