package com.hendisantika.springdataweb.controller;

import com.hendisantika.springdataweb.entity.Book;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-data-web
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 10/08/18
 * Time: 07.11
 * To change this template use File | Settings | File Templates.
 */

@RestController
public class BookController {
    @GetMapping("/book/{id}")
    public ResponseEntity<Book> get(@PathVariable("id") Book book) {
        return Optional.ofNullable(book)
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }
}
